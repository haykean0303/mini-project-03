import React, { useEffect, useState } from "react";
import { useDispatch, useSelector} from "react-redux";
import { deleteArticle, fetchArticle } from "../redux/Action/ArticleAction";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { bindActionCreators } from "redux";
import ReactPaginate from 'react-paginate';
import { strings } from "../localization/localize";
import { GoogleLogin } from 'react-google-login';
import { useHistory } from "react-router";
import { fetchCategory } from "../redux/Action/CategoryAction";

export default function Home() {
  
  const history = useHistory();

  const [profile, setProfile] = useState('')
  const [name, setName] = useState('')

  const [page, setPage] = useState(1)
  const [display, setDisplay] = useState([])

  const dispatch = useDispatch();
  const onDelete = bindActionCreators(deleteArticle, dispatch);
  const article = useSelector((state) => state.ArticleReducer);
  const category = useSelector((state) => state.CategoryReducer.categories);


  const [isArticleLoading, setIsArticleLoading] = useState(true)
  const [isCategoryLoading, setIsCategoryLoading] = useState(true)

  useEffect(() => {
    dispatch(fetchArticle(page)).then(()=>{
      setIsArticleLoading(false)
    });
    dispatch(fetchCategory()).then(()=>{
      setIsCategoryLoading(false)
    });
  }, []);


  const onPageChange= ({selected})=>{
     dispatch(fetchArticle(selected+1))
 }

 const responseGoogle = (response) => {
   setName(response.profileObj.name)
   setProfile(response.profileObj.imageUrl)
 }

  return (
    <Container className="my-4">
 
      <Row>
        
        <Col md="9">
          <h1>{strings.Category}</h1>
          <Row className="my-3">
            {category.map((item, index) => (
                <Button key={index} className="mx-1" variant="outline-secondary" size="sm">{item.name}</Button>
            ))}
          </Row>
          <Row className="my-3">
            {article.articles.map((item, index) => (
              <Col key={index} className="py-2" md="4">
                <Card>
                  <Card.Img variant="top" style={{ objectFit: "cover", height: "150px" }} src={item.image} />
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text className="text-line-3">{item.description}</Card.Text>
                    <Button variant="primary" size='sm' onClick={()=>history.push('/view/'+item._id)}>{strings.Read}</Button>{" "}
                    <Button 
                    variant="warning" 
                    size='sm'
                    onClick={()=>{
                      history.push('/update/article/'+item._id)
                    }}
                    >{strings.Edit}</Button>{" "}
                    <Button variant="danger" size='sm' onClick={()=> onDelete(item._id)}>{strings.Delete}</Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          

        </Col>
      </Row>
  
    </Container>
  );
}
