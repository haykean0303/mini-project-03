import './App.css';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import Author from './views/Author';
import Category from './views/Category';
import Manu from './components/Manu';
import Article from './views/Article';
import Home from './views/Home';
import ViewArticle from './components/ViewArticle';
import { useState } from 'react';
import { Langs } from './util/Langs';

function App() {

  const [lang, setLang] = useState()

  return (
    <div>  
      <Langs.Provider value={{ lang, setLang }}>
      <BrowserRouter>
        <Manu/>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/article' component={Article}/>
          <Route path='/author' component={Author}/>
          <Route path='/category' component={Category}/>
          <Route path='/view/:id' component={ViewArticle}/>
          <Route path='/update/article/:id' component={Article} />
        </Switch>
      </BrowserRouter>
      </Langs.Provider>
    </div>
  );
}

export default App;
